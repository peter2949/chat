from django.db import models


class Message(models.Model):
    """ Model for store users messages. Each user is identified by email """
    email = models.EmailField(blank=False, help_text='User email')
    text = models.TextField(blank=False, help_text='Text message')
    dt_created = models.DateField(auto_now_add=True, help_text='date and time of create message')
    dt_updated = models.DateField(auto_now=True, help_text='date and time of update message')
