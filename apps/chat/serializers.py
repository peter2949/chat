from rest_framework import serializers
import re

from .models import Message


def validate_email(value: any) -> any:
    """ Validates email field """
    if not re.match(r'^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$', value):
        raise serializers.ValidationError('Enter correct email, example: youremail@mail.com')
    print(value)
    return value


def validate_text(value: any) -> any:
    """ Check a text field, must be at least 100 characters """
    if not re.match(r"^\w+", value):
        raise serializers.ValidationError("Message must not be blank")

    if not re.match(r'^(.{1,100})$', value):
        raise serializers.ValidationError('Text length must not be less than 100 characters')
    return value


class MessageSerializer(serializers.ModelSerializer):
    """ Serializer for Message model """
    email = serializers.EmailField(validators=[validate_email])
    text = serializers.CharField(validators=[validate_text])

    class Meta:
        model = Message
        fields = '__all__'
        read_only_fields = ['id']
