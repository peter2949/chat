from random import randint

from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from model_bakery import baker

from .models import Message


class ChatTests(APITestCase):
    """ Test class for testing a Message model, viewing a list of messages and a single """

    def setUp(self):
        """ Set data for testing """
        self.test_message = baker.make(Message)

    def test_message_list(self):
        """ Ensure we get 200 response from API after message list request """
        response = self.client.get(reverse('message_list', args=[self.test_message.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_detail_message(self):
        """ Make sure we get 200 responses from the API after requesting one message """
        response = self.client.get(reverse('message_detail', kwargs={"pk": self.test_message.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_fail_detail_message(self):
        """ Make sure we get 404 responses from the API after requesting a non-existent single message """
        response = self.client.get(reverse('message_detail', kwargs={'pk': self.test_message.id + randint(1, 100)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_message(self):
        """ We get 201 status code when sending a message successfully """
        url = reverse('message_create')
        data = {'email': 'test@gmail.com', 'text': 'test message'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.count(), 2)

    def test_fail_create_message(self):
        """ We get a 400 status code when entering an invalid email """
        url = reverse('message_create')
        data = {'email': 'test@com', 'text': 'text'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
