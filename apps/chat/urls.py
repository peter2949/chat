from django.urls import path
from .views import *


urlpatterns = [
    path('messages/list/<int:page>/', MessageListAPI.as_view(), name='message_list'),
    path('messages/single/<int:pk>/', MessageDetailAPI.as_view(), name='message_detail'),
    path('messages/', MessageCreateAPI.as_view(), name='message_create'),
]
