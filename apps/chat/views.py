from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from django.conf import settings

from .models import Message
from .serializers import MessageSerializer


class MessageListAPI(ListAPIView):
    """ Return list of messages """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get(self, *args, **kwargs):
        page = kwargs['page']

        page_size = settings.PAGINATION_PAGE_SIZE

        messages = Message.objects.all()[page * page_size:(page + 1) * page_size]

        return Response(
            data=MessageSerializer(messages, many=True).data,
            status=HTTP_200_OK,
        )


class MessageDetailAPI(RetrieveAPIView):
    """ Return single message """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MessageCreateAPI(CreateAPIView):
    """ Create message """
    serializer_class = MessageSerializer
